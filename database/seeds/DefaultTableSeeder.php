<?php

use Illuminate\Database\Seeder;

class DefaultTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'Administrador',
            'slug' =>'administrador.sistema',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Seguridad',
            'slug' => 'seguridad',
            'description' => 'Permiso para acceder al modulo seguridad'
        ]);

        DB::table('permission_role')->insert([
            'role_id' => 1,
            'permission_id' => 1,
        ]);

        DB::table('permissions')->insert([
            'name' => 'Seguridad Auditoria',
            'slug' => 'seguridad.auditoria',
            'description' => 'Permiso para acceder al modulo seguridad > auditoria'
        ]);

        DB::table('permission_role')->insert([
            'role_id' => 1,
            'permission_id' => 2,
        ]);

        DB::table('permissions')->insert([
            'name' => 'Seguridad Permisos',
            'slug' => 'seguridad.permisos',
            'description' => 'Permiso para acceder al modulo seguridad > permisos'
        ]);

        DB::table('permission_role')->insert([
            'role_id' => 1,
            'permission_id' => 3,
        ]);

        DB::table('permissions')->insert([
            'name' => 'Seguridad Permisos Crear',
            'slug' => 'seguridad.permisos.include',
            'description' => 'Permiso para agregar nuevo seguridad > permisos'
        ]);

        DB::table('permission_role')->insert([
            'role_id' => 1,
            'permission_id' => 4,
        ]);

        DB::table('permissions')->insert([
            'name' => 'Seguridad Permisos Modificar',
            'slug' => 'seguridad.permisos.update',
            'description' => 'Permiso para editar seguridad > permisos'
        ]);

        DB::table('permission_role')->insert([
            'role_id' => 1,
            'permission_id' => 5,
        ]);

        DB::table('permissions')->insert([
            'name' => 'Seguridad Roles',
            'slug' => 'seguridad.roles',
            'description' => 'Permiso para acceder al modulo seguridad > roles'
        ]);

        DB::table('permission_role')->insert([
            'role_id' => 1,
            'permission_id' => 6,
        ]);

        DB::table('permissions')->insert([
            'name' => 'Seguridad Roles Asignar Permisos',
            'slug' => 'seguridad.roles.asignarPermiso',
            'description' => 'Permiso para acceder asignar permisos a seguridad > roles'
        ]);

        DB::table('permission_role')->insert([
            'role_id' => 1,
            'permission_id' => 7,
        ]);

        DB::table('permissions')->insert([
            'name' => 'Seguridad Roles Modificar',
            'slug' => 'seguridad.roles.update',
            'description' => 'Permiso para modificar seguridad > roles'
        ]);

        DB::table('permission_role')->insert([
            'role_id' => 1,
            'permission_id' => 8,
        ]);

        DB::table('permissions')->insert([
            'name' => 'Seguridad Usuarios Eliminar',
            'slug' => 'seguridad.usuarios.delete',
            'description' => 'Permiso para eliminar seguridad > usuarios'
        ]);

        DB::table('permission_role')->insert([
            'role_id' => 1,
            'permission_id' => 9,
        ]);

        DB::table('permissions')->insert([
            'name' => 'Seguridad Usuarios Crear',
            'slug' => 'seguridad.usuarios.include',
            'description' => 'Permiso para acceder al modulo Incluir Usuarios > auditoria'
        ]);

        DB::table('permission_role')->insert([
            'role_id' => 1,
            'permission_id' => 10,
        ]);

        DB::table('permissions')->insert([
            'name' => 'Seguridad Usuarios Ver Detalles',
            'slug' => 'seguridad.usuarios.details',
            'description' => 'Permiso para acceder al modulo Ver detalles del Usuario'
        ]);

        DB::table('permission_role')->insert([
            'role_id' => 1,
            'permission_id' => 11,
        ]);

        DB::table('permissions')->insert([
            'name' => 'Seguridad Usuarios',
            'slug' => 'seguridad.users',
            'description' => 'Permiso para acceder al modulo seguridad > usuarios'
        ]);

        DB::table('permission_role')->insert([
            'role_id' => 1,
            'permission_id' => 12,
        ]);


        DB::table('permissions')->insert([
            'name' => 'importar excel',
            'slug' => 'importar',
            'description' => 'Permiso para acceder al modulo Importar excel > Importar excel'
        ]);

        DB::table('permission_role')->insert([
            'role_id' => 1,
            'permission_id' => 13,
        ]);

        DB::table('roles')->insert([
            'name' => 'Cortador',
            'slug' => 'cortador',
        ]);

    }
}
