<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'SuperAdmin',
            'slug' =>'superadmin.sistema',
        ]);
        DB::table('users')->insert([
            'username' =>'admin',
            'email' =>'admin@gmail.com',
            'password' => '12345',
            'role_id' => 3,
            'status' => true
        ]);

        DB::table('profiles')->insert([
            'name' => 'Administrador',
            'lastname' =>'Sistema',
            'DNI' => '12345678',
            'license' => '',
            'user_id' => 1,
        ]);
        DB::table('role_user')->insert([
            'role_id' => 3,
            'user_id' => 1,
        ]);

        DB::table('permissions')->insert([
            'name' => 'superadmin',
            'slug' => 'superadmin',
            'description' => 'Permiso para acceder al modulo super administrador'
        ]);

        DB::table('permission_role')->insert([
            'role_id' => 3,
            'permission_id' => 14
        ]);
        DB::table('users')->insert([
            'username' =>'empresa',
            'email' =>'empresa@gmail.com',
            'password' => '12345',
            'role_id' => 1,
            'status' => true
        ]);
        DB::table('profiles')->insert([
            'name' => 'empresa',
            'lastname' =>'empresa',
            'DNI' => '12345679',
            'license' => '',
            'user_id' => 2,
        ]);
        DB::table('role_user')->insert([
            'role_id' => 1,
            'user_id' => 2,
        ]);
/// cortadores
        DB::table('users')->insert([
            'username' =>'alex',
            'email' =>'alex@gmail.com',
            'password' => '12345',
            'role_id' => 2,
            'status' => true
        ]);
        DB::table('profiles')->insert([
            'name' => 'ALEX',
            'lastname' =>'CONDORI JORGE',
            'DNI' => '71044016',
            'license' => 'NA',
            'user_id' => 3,
        ]);
        DB::table('role_user')->insert([
            'role_id' => 2,
            'user_id' => 3,
        ]);
        DB::table('users')->insert([
            'username' =>'eyner',
            'email' =>'eyner@gmail.com',
            'password' => '12345',
            'role_id' => 2,
            'status' => true
        ]);
        DB::table('profiles')->insert([
            'name' => 'eyner leonel',
            'lastname' =>'alania fuertes',
            'DNI' => '10384321',
            'license' => 'NA',
            'user_id' => 4,
        ]);
        DB::table('role_user')->insert([
            'role_id' => 2,
            'user_id' => 4,
        ]);
        DB::table('users')->insert([
            'username' =>'luis',
            'email' =>'luis@gmail.com',
            'password' => '12345',
            'role_id' => 2,
            'status' => true
        ]);
        DB::table('profiles')->insert([
            'name' => 'luis angel',
            'lastname' =>'julca marcheno',
            'DNI' => '47096688',
            'license' => 'NA',
            'user_id' => 5,
        ]);
        DB::table('role_user')->insert([
            'role_id' => 2,
            'user_id' => 5,
        ]);
        DB::table('users')->insert([
            'username' =>'wilder',
            'email' =>'wilder@gmail.com',
            'password' => '12345',
            'role_id' => 2,
            'status' => true
        ]);
        DB::table('profiles')->insert([
            'name' => 'wilder maximo',
            'lastname' =>'guerrero',
            'DNI' => '42717441',
            'license' => 'NA',
            'user_id' => 6,
        ]);
        DB::table('role_user')->insert([
            'role_id' => 2,
            'user_id' => 6,
        ]);
        DB::table('users')->insert([
            'username' =>'kevi',
            'email' =>'kevi@gmail.com',
            'password' => '12345',
            'role_id' => 2,
            'status' => true
        ]);
        DB::table('profiles')->insert([
            'name' => 'kevi alexander',
            'lastname' =>'silva cojal',
            'DNI' => '48112584',
            'license' => 'NA',
            'user_id' => 7,
        ]);
        DB::table('role_user')->insert([
            'role_id' => 2,
            'user_id' => 7,
        ]);

        DB::table('users')->insert([
            'username' =>'kevin',
            'email' =>'kevin@gmail.com',
            'password' => '12345',
            'role_id' => 2,
            'status' => true
        ]);
        DB::table('profiles')->insert([
            'name' => 'kevin',
            'lastname' =>'hoflich moya',
            'DNI' => '72365349',
            'license' => 'NA',
            'user_id' => 8,
        ]);
        DB::table('role_user')->insert([
            'role_id' => 2,
            'user_id' => 8,
        ]);

        DB::table('users')->insert([
            'username' =>'victor',
            'email' =>'victor@gmail.com',
            'password' =>'12345',
            'role_id' => 2,
            'status' => true
        ]);
        DB::table('profiles')->insert([
            'name' => 'victor alberto',
            'lastname' =>'rojas morales',
            'DNI' => '42498688',
            'license' => 'NA',
            'user_id' => 9,
        ]);
        DB::table('role_user')->insert([
            'role_id' => 2,
            'user_id' => 9,
        ]);
        DB::table('users')->insert([
            'username' =>'wilmer1',
            'email' =>'wilmer1@gmail.com',
            'password' => '12345',
            'role_id' => 2,
            'status' => true
        ]);
        DB::table('profiles')->insert([
            'name' => 'wilmer alexander',
            'lastname' =>'carmona diaz',
            'DNI' => '07497967',
            'license' => 'NA',
            'user_id' => 10,
        ]);
        DB::table('role_user')->insert([
            'role_id' => 2,
            'user_id' => 10,
        ]);

        DB::table('users')->insert([
            'username' =>'enrique',
            'email' =>'enrique@gmail.com',
            'password' => '12345',
            'role_id' => 2,
            'status' => true
        ]);
        DB::table('profiles')->insert([
            'name' => 'enrique',
            'lastname' =>'mendez',
            'DNI' => 'Z1',
            'license' => '20600681681',
            'user_id' => 11,
        ]);
        DB::table('role_user')->insert([
            'role_id' => 2,
            'user_id' => 11,
        ]);

        DB::table('users')->insert([
            'username' =>'cesar',
            'email' =>'cesar@gmail.com',
            'password' => '12345',
            'role_id' => 2,
            'status' => true
        ]);
        DB::table('profiles')->insert([
            'name' => 'cesar',
            'lastname' =>'muñoz',
            'DNI' => 'Z2',
            'license' => 'L0001',
            'user_id' => 12,
        ]);
        DB::table('role_user')->insert([
            'role_id' => 2,
            'user_id' => 12,
        ]);
        DB::table('users')->insert([
            'username' =>'ballarta',
            'email' =>'ballarta@gmail.com',
            'password' => '12345',
            'role_id' => 2,
            'status' => true
        ]);
        DB::table('profiles')->insert([
            'name' => 'manual',
            'lastname' =>'ballarta',
            'DNI' => '43982585',
            'license' => 'L0002',
            'user_id' => 13,
        ]);
        DB::table('role_user')->insert([
            'role_id' => 2,
            'user_id' => 13,
        ]);
        DB::table('users')->insert([
        'username' =>'jesus',
        'email' =>'jesus@gmail.com',
        'password' => '12345',
        'role_id' => 2,
        'status' => true
    ]);
        DB::table('profiles')->insert([
            'name' => 'jesus',
            'lastname' =>'colina matheus',
            'DNI' => '20545578418',
            'license' => 'PS 0690785274',
            'user_id' => 14,
        ]);
        DB::table('role_user')->insert([
            'role_id' => 2,
            'user_id' => 14,
        ]);
        DB::table('users')->insert([
            'username' =>'edixon',
            'email' =>'edixon@gmail.com',
            'password' => '12345',
            'role_id' => 2,
            'status' => true
        ]);
        DB::table('profiles')->insert([
            'name' => 'edixon',
            'lastname' =>'perez',
            'DNI' => '001806035',
            'license' => 'NA',
            'user_id' => 15,
        ]);
        DB::table('role_user')->insert([
            'role_id' => 2,
            'user_id' => 15,
        ]);
    }
}
