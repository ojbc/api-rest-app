<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('technician_id')->unsigned()->index();
            $table->integer('order_id')->unsigned()->index();
            $table->integer('cabinet_id')->unsigned()->index();
            $table->string('name')->nullable();
            $table->string('descripcion')->nullable();
            $table->boolean('status');
            $table->boolean('locked');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('technician_id')->references('id')->on('technicians')->onDelete('cascade');
            $table->foreign('cabinet_id')->references('id')->on('cabinets')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
