<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('contract')->unique();
            $table->string('property_unit');
            $table->string('property');
            $table->string('installation');
            $table->double('coordinatex');
            $table->double('coordinatey');
            $table->string('DNI');
            $table->string('full_name');
            $table->string('telephone_contact')->nullable();;
            $table->string('telephone')->nullable();
            $table->string('address')->nullable();
            $table->string('province');
            $table->string('district');
            $table->string('zone');
            $table->string('stratum')->nullable();
            $table->string('date_assignment')->nullable();;
            $table->string('mesh')->nullable();
            $table->string('program')->nullable();
            $table->boolean('status');
            $table->string('descripcion')->nullable();
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
