<?php

namespace App\Http\Controllers;

use App\Repository\OAuth\OAuthClientRepository;
use Carbon\Carbon;
use Laravel\Passport\Token;
use Laravel\Passport\Passport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Utils\Enums\EnumResponse;
use App\Utils\Enums\InventoryError;
use App\Utils\Enums\AuditOperation;
use App\User;
/**
 * Class CustomOAuthController.
 *
 * @package App\Http\Controllers
 * @author  <info@expertosdelaweb.net>
 */
class CustomOAuthController extends Controller
{
    /**
     * @var OAuthClientRepo
     */
    private $oauthClientRepository;

    /**
     * Create a new construct instance.
     * @route rest-api-project\app\Repository\OAuth
     * @param  OAuthClientRepository $repoOAuthClient
     * @return void
     */
    public function __construct( OAuthClientRepository $repoOAuthClient ){
        $this->oauthClientRepository = $repoOAuthClient;
    }

    /**
     * GetAuthorization CustomOAuth.
     * This method get access security at smart asiv services.
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function getAuthorization(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'username'          => 'required',
            'password'          => 'required'
        ]);
        if(! $validator->passes() ) {
            //
            return responseRequest(
                EnumResponse::CUSTOM_FAILED,
                $validator->errors()->keys(),
                InventoryError::INVALID_FIELDS
            );
        }

        if( $request->remember === true ) {
            // Agregamos más días a la expiración del token.
            Passport::tokensExpireIn(Carbon::now()->addDays(30));
            Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));
            Passport::enableImplicitGrant();
        }

        $client = $this->oauthClientRepository->find(2);

        $request->request->add([
            'username' => $request->username,
            'password' => $request->password,
            'grant_type' => 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'scope' => '*'
        ]);

        $proxy = Request::create(
            'oauth/token',
            'POST'
        );

        $data = json_decode( Route::dispatch($proxy)->getContent(), TRUE );

        if( isset( $data['error'] ) ) {
            //
            return responseRequest(
                EnumResponse::FAILED, [],
                InventoryError::INVALID_CREDENTIALS
            );
        }

        // Obtenemos el usuario logueado
        $user = User::where('email', '=', $request->username)->first();
        
        if( $user ) {
            // Vinculamos el user_id
            $data['user_id'] = $user->id;

            // Enviamos el rol del usuario
            $data['role_obj'] = $user->role;
            $data['role'] = $user->role->slug;

            // Enviamos los permisos del usuario
            $data['permissions'] = $user->getPermissions();

            // CREAMOS LA AUDITORIA.
            auditSecurity( $user->id, AuditOperation::LOGIN, 'ACCESS' );

            // Devolvemos
            return responseRequest( EnumResponse::SUCCESS, $data );
        }
    }


    /**
     * RevokeAuthorization CustomOAuth.
     * This function a revoke access token security.
     * @param Request $request
     * @return bodyResponseRequest 'Sesión finalizada exitosamente'
     */
    public function revokeAuthorization(Request $request)
    {
        try {

            $user_id = $request->user_id;
            $clientId = $request->client_id;
            
            $queries = Token::where('user_id', $user_id)
                ->where('client_id', $clientId)->get();
            
            foreach ($queries as $query) {
                $token = Token::find($query->id);
                $token->revoked = true;
                $token->save();
            }

            // GENERATED AUDIT ACCESS
            auditSecurity( $request->user_id, AuditOperation::LOGOUT, 'ACCESS' );

            return bodyResponseRequest(
                EnumResponse::CUSTOM_SUCCESS, [],
                'Sesión finalizada exitosamente'
            );

        } catch (\Exception $ex) {
            return bodyResponseRequest(EnumResponse::ERROR, $ex, [], 'CustomOAuthController.getAuthorization.catch');
        }
    }
}

