<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repository\Client\ClientRepository;
use App\Utils\Enums\EnumResponse;
use App\Utils\Enums\InventoryError;
use App\User;
use App\Client;
use Carbon\Carbon;
use Excel;
use Illuminate\Support\Facades\DB;
/**
 * Class ClientsController.
 *
 * @package App\Http\Controllers
 */
class ClientsController extends Controller
{
    /**
     * Create a new construct instance.
     * @route api-rest-app\app\Repository\Client
     * @param  ClientRepository $_clientRepository
     * @return void
     */
    public function __construct( ClientRepository $_clientRepository )
    {
        $this->clientRepository = $_clientRepository;
    }

    public function save(Request $request)
    {
        \Log::debug('  $request', [  $request]);
        if ($request->file('archive')->isValid()) {
            $file = $request->file('archive');

            $user = $request->get('user_id');

            $alm = [];
            $data =[];
            $cont= 0;



                $file = $request->file('archive');

                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');
                })->all();



                if (!empty($excelData)) {


                    foreach ($excelData as $key => $values) {
                        \Log::debug(' $values', [ $values]);
                    }


                }



        }else{
            return bodyResponseRequest( EnumResponse::ERROR,'' , [], 'ClientsController.save.catch' );
        }


    }



}