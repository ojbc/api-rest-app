<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repository\User\UserRepository;
use App\Utils\Enums\EnumResponse;
use App\User;
/**
 * Class UsersController.
 *
 * @package App\Http\Controllers
 */
class UsersController extends Controller
{
    /**
     * Create a new construct instance.
     * @route rest-api-project\app\Repository\User
     * @param  UserRepository $_userRepository
     * @return void
     */
    public function __construct( UserRepository $_userRepository )
    {
        $this->userRepository = $_userRepository;
    }
    /**
     * Profile User.
     *
     * @param
     * @return bodyResponseRequest $data
     */
    public function profile($user)
    {
        try {
            $data = User::where('id', '=',$user->id)
                ->with(['profile' => function( $query ) {
                }])->orderBy('created_at', 'DESC')->get();
               
            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'ManagerController.profile.catch' );
        }
    }
    /**
     * All Users.
     *
     * @param
     * @return bodyResponseRequest $data
     */
    public function all()
    {
        try {

            $data = $this->userRepository->allWith(['profile', 'role'])->orderBy('created_at', 'DESC')->get();

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'UsersController.all.catch' );
        }
    }
    /**
     * GetFullDetails Users.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function getFullDetails( Request $request )
    {
        try {

            $data = $this->userRepository->findWith( $request->user_id, ['profile', 'role', 'permissions']);

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'UsersController.all.catch' );
        }
    }
    /**
     * ChangeRole Users.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function changeRole( Request $request )
    {
        try {

            $data = $this->userRepository->changeRole( $request );

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'UsersController.changeRole.catch' );
        }
    }

    /**
     * login Users.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function login( Request $request )
    {
        try {
            \Log::debug('$request', [$request]);
            $data = User::where('username', '=', $request->username)->where('password', '=', $request->password)
                ->with(['profile' => function( $query ) {
                }])->orderBy('created_at', 'DESC')->get();

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'ManagerController.login.catch' );
        }
    }




}