<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Utils\Enums\EnumResponse;
use App\Utils\Enums\InventoryError;
use App\Utils\Enums\AuditOperation;
/**
 * Class AuthController.
 *
 * @package App\Http\Controllers
 *
 */
class AuthController extends Controller
{
    /**
     * .
     *
     * Auth AuthController.
     *
     * @param Request $request
     * @return responseRequest $data
     */
    public function auth(Request $request)
    {

        $credentials = $request->only('username', 'password');
        $token = null;

        try {
            if(!$token = JWTAuth::attempt($credentials)) {

                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {

            return response()->json(['error' => 'something_went_wrong e:' . $e], 500);
        }

        // Obtenemos el usuario logueado
        $user = JWTAuth::toUser($token);

        if( $user ) {

            $data = [
                'name_id' => $user->profile->full_name,
                'user_id' => $user->id,
                'role_obj' => $user->role,
                'role' => $user->role->slug,
                'permissions' => $user->getPermissions(),
                'token' => $token
            ];


            // CREAMOS LA AUDITORIA.
            auditSecurity( $user->id, AuditOperation::LOGIN, 'ACCESS' );

            // Devolvemos
            return responseRequest( EnumResponse::SUCCESS, $data );
        }
    }
    /**
     * .
     *
     * Logout AuthController.
     *
     * @param Request $request
     * @return responseRequest
     */
    public function logout(Request $request)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();

            // GENERATED AUDIT ACCESS
            auditSecurity( $user->id, AuditOperation::LOGOUT, 'ACCESS' );
            //
            JWTAuth::invalidate( JWTAuth::getToken() );

            return bodyResponseRequest( EnumResponse::CUSTOM_SUCCESS, [], 'Sesión finalizada exitosamente' );
        } catch (\Exception $ex) {
            return bodyResponseRequest(EnumResponse::ERROR, $ex, [], 'AuthController.logout.catch');
        }

    }
}