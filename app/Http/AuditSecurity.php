<?php
use App\Repository\AuditSecurity\AuditRepository;
use App\Audit;

if (! function_exists('auditSecurity') ) {

    /**
     * This function a create audit security.
     *
     * @param int $user value of identity user login/logout
     * @param int $typeaudit value of type audit
     * @param int $typeaccess value of type access user
     * @param string $ip value of ip address ej: '182.168.010.125'
     */
    function auditSecurity( $user_id, $operation, $module, $model = null, $details = '{}' ){
        // Instanciamos
        $_audit = new AuditRepository;
        // Creamos la aditoría
        $_audit->create([
            'user_id' => $user_id,
            'type_operation' => $operation,
            'module' => $module,
            'model' => $model,
            'details' => json_encode($details)
        ]);
        // Retornamos los detalles
        return $_audit;
    }
}
