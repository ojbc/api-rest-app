<?php
/**
 * Created by PhpStorm.
 * User: Desarrollo
 * Date: 22/10/2018
 * Time: 3:34 PM
 */

namespace App\Utils\Enums;


class EnumResourceStatus
{
    /**
     * @var int
     */
    const PENDING = 1;

    /**
     * @var int
     */
    const APPROVED = 2;

    /**
     * @var int
     */
    const REJECTED = 3;
}