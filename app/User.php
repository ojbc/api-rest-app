<?php
namespace App;
use Ultraware\Roles\Traits\HasRoleAndPermission;
use Ultraware\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

use Ultraware\Roles\Models\Permission;
use Ultraware\Roles\Models\Role;

class User extends Authenticatable implements HasRoleAndPermissionContract
{
    use HasApiTokens, Notifiable, HasRoleAndPermission;

    /**
     * Tabla asociada al modelo.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'company_id', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    //protected $appends = ['role_permissions'];
    protected $appends = ['permissions_extras'];

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function role()
    {
        return $this->belongsTo(config('roles.models.role'), 'role_id');
    }

    public function permissions()
    {
        return $this->belongsToMany(config('roles.models.permission'), 'permission_user', 'user_id', 'permission_id')->withTimestamps();
    }


    public function getRolePermissionsAttribute()
    {
        return \DB::table('permissions')
            ->join('permission_role', 'permission_role.permission_id', '=', 'permissions.id')
            ->where('permission_role.role_id', $this->role_id);
    }

    public function getPermissionsExtrasAttribute()
    {
        return \DB::table('permissions')
            ->whereNotIn('id', $this->getRolePermissionsAttribute()
                ->pluck('permissions.id'))->get();
    }

}
