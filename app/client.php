<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * Tabla asociada al modelo.
     *
     * @var string
     */
    protected $table = 'clients';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contract', 'property_unit', 'property', 'installation', 'coordinatex', 'coordinatey', 'DNI', 'full_name', 'telephone_contact', 'telephone', 'address', 'province', 'district', 'zone', 'stratum', 'date_assignment', 'mesh', 'program', 'status', 'descripcion'
  ];
    protected $dates = [
        'created_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function profile()
    {
        return $this->belongsTo('App\Profile', 'user_id', 'user_id');
    }
}