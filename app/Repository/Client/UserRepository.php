<?php
namespace App\Repository\Client;
use App\User;
use App\Profile;
use App\Client;
use Illuminate\Support\Facades\DB;
use App\Repository\Base\BaseRepository;
use App\Utils\Enums\AuditOperation;

/**
 * Class ClientRepository.
 *
 * @package App\Repository\Client
 *
 */
class ClientRepository extends BaseRepository
{
    /**
     * ClientRepository construct.
     * 
     * @param Client $model
     * @return void
     */
    public function __construct(Client $model)
    {
        parent::__construct($model);
    }


}
