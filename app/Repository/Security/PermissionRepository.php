<?php
namespace App\Repository\Security;
use Illuminate\Support\Facades\Auth;
use App\Repository\Base\BaseRepository;
use Ultraware\Roles\Models\Permission;
use Ultraware\Roles\Models\Role;
use App\Utils\Enums\AuditOperation;
/**
 * Class PermissionRepository.
 *
 * @package App\Repository\Security
 * @author  <info@expertosdelaweb.net> 
 */
class PermissionRepository extends BaseRepository
{
    /**
     * PermissionRepository construct.
     * 
     * @param Permission $model
     * @return void
     */
    public function __construct( Permission $model )
    {
        parent::__construct( $model );
    }
    /**
     * SavePermission Permission.
     *
     * @param $request
     * @return $perm
     */
    public function savePermission( $form )
    {   
        // Creamos la data
        $data = [
            'name' => $form['name'],
            'slug' => $form['slug'],
            'description' => $form['description']
        ];
        
        if( empty($form['old']) ):
            // Creamos el permiso
            $perm = Permission::create( $data );

            // Vinculamos el permiso al rol Administrador
            $role = Role::find(1); // 1 => Administrador
            $role->attachPermission($perm);

            // CREAMOS LA AUDITORIA.
            auditSecurity( Auth::id(), AuditOperation::CREATE, 'SECURITY.PERMISSIONS', 'Ultraware\Roles\Models\Permission', ['id' => $perm->id] );
        else:
            $perm = Permission::where( 'slug', $form['old']['slug'] )->first();
            // Verificamos si existe el permiso
            if( $perm ) {
                // CREAMOS LA AUDITORIA.
                auditSecurity( Auth::id(), AuditOperation::UPDATE, 'SECURITY.PERMISSIONS', 'Ultraware\Roles\Models\Permission', [ 'id' => $perm->id, 'old' => $perm, 'new' => $data] );

                // Guardamos los datos.
                $perm->fill( $data );
                $perm->save();
            }
        endif;
        return $perm;
    }
}