<?php
namespace App\Repository\Security;
use App\Repository\Base\BaseRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Ultraware\Roles\Models\Role;
use Ultraware\Roles\Models\Permission;
use App\Utils\Enums\AuditOperation;
/**
 * Class RoleRepository.
 *
 * @package App\Repository\Security
 * @author  <info@expertosdelaweb.net> 
 */
class RoleRepository extends BaseRepository
{
    /**
     * RolRepository construct.
     * 
     * @param Role $model
     * @return void
     */
    public function __construct( Role $model )
    {
        parent::__construct( $model );
    }
    /**
     * GetPermission Role.
     *
     * @param $request
     * @return $data
     */
    public function getPermission( $slug )
    {   
        $data = collect([]);
        $rol = Role::where('slug', '=', $slug)->first();
        $permissions = Permission::all();

        foreach ( $permissions as $perm ) {
            //
            $perm_role = DB::table('permission_role')
                ->where('permission_id', '=', $perm->id)
                ->where('role_id', '=', $rol->id)
                ->count();

            if( $perm_role > 0 ) {
                //
                $perm['checked'] = true;
            }
            //
            $data->push($perm);
        }

        return $data;
    }
    /**
     * TogglePermission Role.
     *
     * @param $request
     * @return ['permissions' => $perm,'role' => $rol]
     */
    public function togglePermission( $request )
    {
        // Obtenemos el rol
        $rol = Role::where( 'slug', '=', $request->slug )->first();
        $perm = Permission::where( 'slug', '=', $request->perm )->first();

        // Verificamos
        if( $rol ) {
            if( $perm ) {
                // Verificamos
                $perm_role = DB::table('permission_role')
                    ->where('permission_id', '=', $perm->id)
                    ->where('role_id', '=', $rol->id)
                    ->count();      
                // ¿existe?    
                if( $perm_role <= 0 ) {
                    // Añadimos el permiso
                    $rol->attachPermission( $perm );

                    // CREAMOS LA AUDITORIA.
                    auditSecurity( Auth::id(), AuditOperation::UPDATE, 'SECURITY.ROLES', 'Ultraware\Roles\Models\Permission', ['id' => $perm->id, 'active' => 1] );
                } else {
                    // Quitamos el permiso
                    $rol->detachPermission( $perm );

                    // CREAMOS LA AUDITORIA.
                    auditSecurity( Auth::id(), AuditOperation::UPDATE, 'SECURITY.ROLES', 'Ultraware\Roles\Models\Permission', ['id' => $perm->id, 'active' => 0] );
                }

                return [ 
                    'permissions' => $perm,
                    'role' => $rol
                ];
            } else {
                // Devolvemos un error
                return bodyResponseRequest( EnumResponse::FAILED );
            }
        } else {
            // Devolvemos un error
            return bodyResponseRequest( EnumResponse::FAILED );
        }
    }
    /**
     * SaveRole Role.
     *
     * @param $request
     * @return $role
     */
    public function saveRole( $form )
    {   
        // Obtenemos el slug
        $slug = str_slug( $form['name'], '.' );

        // Creamos la data
        $data = [
            'name' => $form['name'],
            'slug' => $slug,
            'description' => $form['description']
        ];
        
        if( empty($form['slug']) ):
            // Creamos el rol
            $role = Role::create( $data );

            // CREAMOS LA AUDITORIA.
            auditSecurity( Auth::id(), AuditOperation::CREATE, 'SECURITY.ROLES', 'Ultraware\Roles\Models\Role', [ 'id' => $role->id] );
        else:
            $role = Role::where( 'slug', '=' , $form['slug'] )->first();
            // Verificamos
            if( $role ) {
                // CREAMOS LA AUDITORIA.
                auditSecurity( Auth::id(), AuditOperation::UPDATE, 'SECURITY.ROLES', 'Ultraware\Roles\Models\Role', [ 'id' => $role->id, 'old' => $role, 'new' => $data ] );

                // Guardamos la nueva data
                $role->fill( $data );
                $role->save();
            }
        endif;

        //
        return $role;
    }
}