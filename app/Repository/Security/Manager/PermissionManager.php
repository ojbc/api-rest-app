<?php

namespace App\Repository\Security\Manager;

use App\Utils\BaseManager;
use Illuminate\Validation\Rule;
/**
 * Class RoleManager
 *
 * @package App\Repository\Security\Manage;
 * @author Alejandro Pérez <alejandroprz2011@gmail.com>
 */
class PermissionManager extends BaseManager
{
     /**
     * AddressManager constructor.
     *
     * @param array|mixed $data
     */
    public function __construct($data)
    {
        parent::__construct($data);
    }

    /**
     * @inheritdoc
     */
    protected function onValidate()
    {
        return [
            'name'=>'required',
            'slug' => [
                'required',
                Rule::exists('permissions')->where(function ($query) {
                    $query->where('slug', $this->input('slug'));
                }),
            ],
            'description'=>'required'
        ];
    }
}