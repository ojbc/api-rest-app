<?php

namespace App\Repository\User\Manager;

use App\Utils\BaseManager;

/**
 * Class UserManager
 *
 * @package App\Repository\User\Manage;
 * @author Alejandro Pérez <alejandroprz2011@gmail.com>
 */
class UserManager extends BaseManager
{
     /**
     * AddressManager constructor.
     *
     * @param array|mixed $data
     */
    public function __construct($data)
    {
        parent::__construct($data);
    }

    /**
     * @inheritdoc
     */
    protected function onValidate()
    {
        return [
            'username'  =>  [
                'required',
                Rule::exists('users')->where(function ($query) {
                    if( isset( $this->username ) )
                        $query->where('username', $this->username);
                }),
            ],
            'nombres'  =>  'required',
            'apellidos' => 'required',
            'sexo' => 'required',
            'role' => 'required',
            'dia' => 'required',
            'mes' => 'required',
            'anio' => 'required',
            'password' => 'required'
        ];
    }
}