<?php
namespace App\Repository\User;
use App\User;
use App\Profile;
use Illuminate\Support\Facades\DB;
use Ultraware\Roles\Models\Role;
use App\Repository\Base\BaseRepository;
use App\Utils\Enums\AuditOperation;
use Illuminate\Support\Facades\Auth;
/**
 * Class UserRepository.
 *
 * @package App\Repository\User
 *
 */
class UserRepository extends BaseRepository
{
    /**
     * UserRepository construct.
     * 
     * @param User $model
     * @return void
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }
    /**
     * FindUsers User.
     *
     * @param array $params
     * @return $users->get()
     */
    public function findUsers(array $params)
    {
        $users =  $this->model->select(
            'users.id',
            'profiles.name as names',
            'profiles.lastname as lastnames',
            'profiles.DNI',
            'roles.name as rol_name'
        )
        ->join('profiles', 'users.id', '=', 'profiles.user_id')
        ->join('role_user', 'users.id', '=', 'role_user.user_id')
        ->join('roles', 'role_user.role_id', '=', 'roles.id');

        if (isset($params['username'])) {
           $users = $users->where('users.username', 'LIKE', '%' . $params['username'] . '%');
        }

        if (isset($params['role'])) {
            $users = $users->where('roles.id', $params['role']);
        }

        return $users->get();
    }
    /**
     * ChangeRole User.
     *
     * @param $request
     * @return $user
     */
    public function changeRole( $request )
    {
        // Obtenemos el usuario
        $user = User::find( $request->user );

        $old_role = $user->role();

        // Obtenemos el rol a establecer
        $rol = Role::where( 'slug', '=' , $request->role )->first();

        // CREAMOS LA AUDITORIA.
        auditSecurity( Auth::id(), AuditOperation::UPDATE, 'SECURITY.USERS', 'User', [ 'id' => $user->id, 'old' => $old_role, 'new' => $rol ]);

        // Eliminamos todos los roles del usuario
        $user->detachAllRoles();
        // Le añadimos el rol al usuario.
        $user->role_id = $rol->id;
        $user->attachRole( $rol );
        $user->save();

        // retornamos el usuario
        return $user;
    }

    /**
     * CheckUserByRol User.
     * This function a ckeck user by rol.
     * @param 
     * @return boolean
     */
    public function checkUserByRol()
    {
        return true;
    }
}
