<?php
namespace App\Repository\AuditSecurity;
use App\Audit;
use App\Repository\Base\BaseRepository;

/**
 * Class AuditSecurityRepo
 * 
 * @package App\Repository\AuditSecurity
 * @author  <info@expertosdelaweb.net>
 */
class AuditRepository extends BaseRepository
{
    /**
     * AdjustmentRepository construct.
     * 
     * @param Audit $model
     * @return void
     */
    public function __construct()
    {
        parent::__construct( new Audit );
    }
}