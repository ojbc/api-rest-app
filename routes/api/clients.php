<?php
Route::group(['prefix' => 'client','middleware'=> ['cors']], function () {

    Route::post('save', [
        'as'=> 'clients.save',
        'uses' => 'clientsController@save'
    ]);

});