<?php
Route::group(['prefix' => 'user','middleware'=> ['cors']], function () {

    Route::post('login', [
        'as'=> 'users.login',
        'uses' => 'UsersController@login'
    ]);
    Route::get('Profile', [
        'as'=> 'users.profile',
        'uses' => 'UsersController@profile'
    ]);
});